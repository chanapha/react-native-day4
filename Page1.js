import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Text, TouchableOpacity } from "react-native";

 class Page1 extends Component {
  render() {
    const { todos, completes, addTodo } = this.props
    return (
      <View style={{ flex: 1 }}>
        <TouchableOpacity onPress={() => {addTodo(1)}}>
          <Text>Add Number to TODO</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {addTodo(7)}}>
          <Text>Add Number to TODO</Text>
        </TouchableOpacity>
        <Text>Latest todo is: {todos[todos.length - 1]}</Text>
      </View>
    );
  }
}
 const mapStateToProps = (state) => {
     return {
         todos: state.todos,
         completes: state.complete
     }
 }
 
 const mapDispatchToProps =(dispatch) => {
     return{
         addTodo: (topic) => {
             dispatch({
                 type: 'ADD_TODO',
                 topic: topic
             })
         }
     }
 }

 export default connect(mapStateToProps, mapDispatchToProps)(Page1)